<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- Exo 1 -->
<?php 

$number= 0;
while ($number <= 10) {
    echo $number++;
    echo '<br />';
}
?>

<!-- Exo 2 -->
<?php

$a=0;
$varNumber= rand(1,100);

while ($a <= 20) {
echo $a*$varNumber;
$a++;
echo '<br />';
}
?>

<!-- Exo 3 -->

<?php
$var1 = 100;
$var2= rand(1,100);

 while ($var1 <=10) {
     echo $var1*$var2;
    
     echo '<br />';
 
    }

?>

<!-- Exo 4 -->

<?php

$number= 1;

while($number <= 10) {
echo "$number<br/>"; 
$number += ($number/2);

}
 ?>
<?php

// Exo 5:En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...
$number1= 1;
while ($number1 < 15) {
    $number1++;
    echo " $number1.On y arrive presque<br />";
}

?>

<?php

// Exo 6:  En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...
$myVar= 20;
while ($myVar > 0) {
    $myVar--;
    echo "$myVar.C'est presque bon<br />";
}
?>

<?php 

// Exo 7 : En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...

$myVar1= 1;
while ($myVar1 < 100) {
    $myVar1+= 15;
    echo "$myVar1.On tient le bon bout<br />";
}
?>

<?php

// Exo 8: En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !
$myVar2= 200;
while ($myVar2 > 0) {
    $myVar2-= 12;
    echo "$myVar2.Enfin ! ! !<br />";
}
?>











</body>
</html>
